package csd562.csdp1092.bonus;

import java.io.File;
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import csd562.csdp1092.Shared.DoubleWritableCustom;
import csd562.csdp1092.Shared.Utils;

public class Pairs {
	
    public static class TokenizeMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);
        private Text pair = new Text();
        
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        	String delimiter = " !@#$%^&*()-_=+,./?:;'\"|{}[]~`<>\t\r\n";
        	StringTokenizer strtok = new StringTokenizer(value.toString(), delimiter); 
        	String[] lineWords = new String[strtok.countTokens()];
        	int count = 0;
        	while(strtok.hasMoreTokens()){
        		String word = strtok.nextToken();
        		if(!Character.isLetter(word.charAt(0)))
        			continue;
        		lineWords[count++] = word;
        		
        	}
        	for(int i=0; i < lineWords.length - 1; i++){
        		for(int j = i+1; j < lineWords.length; j++){
        			pair.set(lineWords[i] + "," + lineWords[j]);
        			context.write(pair, one);
        		}
        	}
        }
    }

    public static class IntSumReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
        private IntWritable result = new IntWritable();
        
        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;
        	for(IntWritable value : values){
            	sum += value.get();
            }
        	result.set(sum);
        	context.write(key, result);
        }
    }
    
    public static class CountMapper extends Mapper<LongWritable, Text, Text, DoubleWritable> {

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        	String[] pairParts = value.toString().split("[ ,\t\n]");
        	Text pair = new Text(pairParts[0] + "," + pairParts[1]);
        	DoubleWritable dw = new DoubleWritable(Double.parseDouble(pairParts[2]));
        	System.out.println(dw.get());
        	context.write(new Text(pairParts[0]), dw);
        	context.write(pair, dw);
        }
        
    }
    
    public static class RelFreqReducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {
        private DoubleWritable result = new DoubleWritable();
        private static double countA;
        
        @Override
        protected void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
        	String[] keyParts = key.toString().split("[ ,\t\n]");
        	if(keyParts.length == 1){
        		double sum = 0;
        		for(DoubleWritable dw : values){
        			sum += dw.get();
        		}
        		countA = sum;

        		return;
        	}
            double val = values.iterator().next().get();
        	result.set(val / countA);
        	context.write(key, result);
        }
    }
    
    public static class ReverseOrderMapper extends Mapper<LongWritable, Text, DoubleWritableCustom, Text> {

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        	String[] pairParts = value.toString().split("[ ,\t\n]");
        	Text pair = new Text(pairParts[0] + "," + pairParts[1]);
        	DoubleWritableCustom dw = new DoubleWritableCustom();
        	dw.set(Double.parseDouble(pairParts[2]));
        	context.write(dw, pair);
        }
        
    }
    
    public static class PairOutputReducer extends Reducer<DoubleWritableCustom, Text, Text, DoubleWritableCustom> {
        
        @Override
        protected void reduce(DoubleWritableCustom key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        	for(Text value : values){
        		context.write(value, key);
            }        	
        }
    }

    public static void main(String[] args) throws Exception {
    	Configuration conf = new Configuration();
    	
    	Path out1 = new Path(args[1]);
    	Path out2 = new Path(args[2]);
    	Path pairsOut = new Path(args[3]);

    	
    	FileSystem fs = FileSystem.get(conf);
    	fs.delete(out1);
    	fs.delete(out2);
    	fs.delete(pairsOut);
    	
    	Job job1 = new Job(conf, "out1");
    	job1.setJarByClass(Pairs.class);
    	job1.setMapperClass(TokenizeMapper.class);
    	job1.setReducerClass(IntSumReducer.class);
    	job1.setOutputKeyClass(Text.class);
    	job1.setOutputValueClass(IntWritable.class);
    	FileInputFormat.addInputPath(job1, new Path(args[0]));
    	FileOutputFormat.setOutputPath(job1, out1);
    	
        boolean success = job1.waitForCompletion(true);
        if(!success)
        	System.exit(1);
        
        Job job2 = new Job(conf, "out2");
    	job2.setJarByClass(Pairs.class);
    	job2.setMapperClass(CountMapper.class);
    	job2.setReducerClass(RelFreqReducer.class);
    	job2.setOutputKeyClass(Text.class);
    	job2.setOutputValueClass(DoubleWritable.class);
    	FileInputFormat.addInputPath(job2, out1);
    	FileOutputFormat.setOutputPath(job2, out2);
    	
        boolean success2 = job2.waitForCompletion(true);
        if(!success2)
        	System.exit(1);
        
        Job job3 = new Job(conf, "Pairs");
    	job3.setJarByClass(Pairs.class);
    	job3.setMapperClass(ReverseOrderMapper.class);
    	job3.setReducerClass(PairOutputReducer.class);
    	job3.setOutputKeyClass(DoubleWritableCustom.class);
    	job3.setOutputValueClass(Text.class);
    	FileInputFormat.addInputPath(job3, out2);
    	FileOutputFormat.setOutputPath(job3, pairsOut);
    	
        boolean success3 = job3.waitForCompletion(true);
        if(!success3)
        	System.exit(1);
        
        Utils utils = new Utils();
        utils.printOutputFolder(new File(pairsOut.getName()), 100);
        
    	fs.delete(out1);
    	fs.delete(out2);
    	
        fs.close();
    }
}
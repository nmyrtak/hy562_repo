package csd562.csdp1092.exercise2b;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapred.Counters.Counter;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import com.google.common.base.Joiner;

import csd562.csdp1092.Shared.IntWritableCustom;
import csd562.csdp1092.Shared.Utils;
import csd562.csdp1092.exercise1.StopWordsFinder;
import csd562.csdp1092.exercise1.StopWordsFinder.IntSumReducer;
import csd562.csdp1092.exercise1.StopWordsFinder.TokenizeMapper;

public class InvertedIndex {
	
	private static enum UnqDocsCounter {
	    UNQ_DOC_WORDS
	}
	
	public static class WordsDocumentsMapper extends Mapper<LongWritable, Text, Text, Text> {
		
		private Text word = new Text();
		private Text doc = new Text();
		
		@Override
		protected void map(LongWritable key, Text value,Context context) throws IOException, InterruptedException {
			String fileName = ((org.apache.hadoop.mapreduce.lib.input.FileSplit) context.getInputSplit()).getPath().getName();
			String delimiter = " !@#$%^&*()-_=+,./?:;'\"|{}[]~`<>\t\r\n";
        	StringTokenizer strtok = new StringTokenizer(value.toString(), delimiter);
        	while(strtok.hasMoreTokens()){
        		String token = strtok.nextToken().toLowerCase();
        		word.set(token);
        		doc.set(fileName);
        		context.write(word, doc);
        	}
		}
		
	}
	
	public static class WordDocsWriteReducer extends Reducer<Text, Text, Text, Text>{
		
		private Text result = new Text();
		
		@Override
		protected void reduce(Text wordKey, Iterable<Text> docValues,Context context) throws IOException, InterruptedException {
			StringBuilder sb = new StringBuilder();
			for(Text doc : docValues){
				sb.append(doc.toString()).append(",");
			}
			sb.deleteCharAt(sb.lastIndexOf(","));
			result.set(sb.toString());
			context.write(wordKey, result);
		}
		
		
	}
	
	public static class StopWordsFilterMapper extends Mapper<LongWritable, Text, Text, Text> {
		
		Text word = new Text();
		Text docs = new Text();
		
		private static HashSet<String> stopWordsSet;
		
		@Override
		protected void map(LongWritable key, Text line,Context context) throws IOException, InterruptedException {
			String[] lineParts = line.toString().split("[ ,\t\n]");
			int wordIndex = 0;
			if(stopWordsSet.contains(lineParts[wordIndex]) || !Character.isLetter(lineParts[wordIndex].charAt(0)))
    			return;
			StringBuilder sb = new StringBuilder();
			for(int i = wordIndex + 1; i < lineParts.length; i++)
				sb.append(lineParts[i]).append(",");
			sb.deleteCharAt(sb.lastIndexOf(","));
			word.set(lineParts[wordIndex]);
			docs.set(sb.toString());
			context.write(word, docs);
		}
		
		
		protected void setup(Context context) throws IOException, InterruptedException {
			stopWordsSet = new HashSet<>();
			URI[] localPaths = context.getCacheFiles();
			FileSystem fs = FileSystem.get(context.getConfiguration());
			FSDataInputStream fsdi = fs.open(new Path(localPaths[0].toString()));
			String line = "";
			while((line = fsdi.readLine()) != null){
				int wordIndex = 0;
				String[] lineParts = line.split("[ ,\n]");
				stopWordsSet.add(lineParts[wordIndex]);
			}
		}

	}

	public static class InvIndexWriteReducer extends Reducer<Text, Text, Text, Text>{
		
		private Text result = new Text();
		private Text id = new Text();
		
		@Override
		protected void reduce(Text wordKey, Iterable<Text> docValues,Context context) throws IOException, InterruptedException {
			org.apache.hadoop.mapreduce.Counter c = context.getCounter(org.apache.hadoop.mapred.Task.Counter.REDUCE_INPUT_GROUPS);
			long ID = c.getValue();
			StringBuilder unqDocs = new StringBuilder();
			String allDocsAsStr = docValues.iterator().next().toString();
			String[] allDocsArr = allDocsAsStr.split("[ ,\t\n]");
			HashSet<String> docsSet = new HashSet<>();
			for(String currDoc : allDocsArr){
				docsSet.add(currDoc);
			}
			if(docsSet.size() == 1)
				context.getCounter(UnqDocsCounter.UNQ_DOC_WORDS).increment(1);
			for(String doc : docsSet){
				unqDocs.append(doc).append(",");
			}
			unqDocs.deleteCharAt(unqDocs.lastIndexOf(","));
			result.set(wordKey + "\t" + unqDocs);
			id.set(ID + "");
			context.write(id, result);
		}
		
	}
   

    public static void main(String[] args) throws Exception {

    	Path input = new Path(args[0]);
    	Path wordsDocsTuplesPath = new Path("wordsDocsTupleOut");
    	Path invertedIndexPath = new Path(args[1]);
    	
    	Configuration conf = new Configuration();
    	
    	FileSystem fs = FileSystem.get(conf);
    	fs.delete(wordsDocsTuplesPath);
    	fs.delete(invertedIndexPath);
    	
    	Job wordDocsTuplesJob = new Job(conf, "word docs tuples");
    	wordDocsTuplesJob.setJarByClass(InvertedIndex.class);
    	wordDocsTuplesJob.setMapperClass(WordsDocumentsMapper.class);
    	wordDocsTuplesJob.setReducerClass(WordDocsWriteReducer.class);
    	wordDocsTuplesJob.setOutputKeyClass(Text.class);
    	wordDocsTuplesJob.setOutputValueClass(Text.class);
    	FileInputFormat.addInputPath(wordDocsTuplesJob, new Path(args[0]));
    	FileOutputFormat.setOutputPath(wordDocsTuplesJob, wordsDocsTuplesPath);
        wordDocsTuplesJob.waitForCompletion(true);
       
        System.out.println("SUCCESS");
        
        Job invIndexJob = Job.getInstance(conf);
    	invIndexJob.addCacheFile(new URI("stopwords.csv"));
    	invIndexJob.setJarByClass(InvertedIndex.class);
    	invIndexJob.setMapperClass(StopWordsFilterMapper.class);
    	invIndexJob.setReducerClass(InvIndexWriteReducer.class);
    	invIndexJob.setOutputKeyClass(Text.class);
    	invIndexJob.setOutputValueClass(Text.class);
    	FileInputFormat.addInputPath(invIndexJob, wordsDocsTuplesPath);
    	FileOutputFormat.setOutputPath(invIndexJob, invertedIndexPath);
    	invIndexJob.waitForCompletion(true);
    	
    	
    	Utils utils = new Utils();
    	//utils.printOutputFolder(new File(wordsDocsTuplesPath.getName()));
    	//utils.printOutputFolder(new File(invertedIndexPath.getName()));
    	
    	org.apache.hadoop.mapreduce.Counter c1 = invIndexJob.getCounters().findCounter(org.apache.hadoop.mapred.Task.Counter.REDUCE_OUTPUT_RECORDS);
    	org.apache.hadoop.mapreduce.Counter unqCounter = invIndexJob.getCounters().findCounter(UnqDocsCounter.UNQ_DOC_WORDS);

    	System.out.println("Words of index counter " + c1.getValue());
    	System.out.println("Words appear in unique documents counter " + unqCounter.getValue());

    	utils.storeUnqCounter(conf, unqCounter.getValue());
    	
    	fs.delete(wordsDocsTuplesPath);
    	fs.close();
    
    }
    
    
    
    
    
}
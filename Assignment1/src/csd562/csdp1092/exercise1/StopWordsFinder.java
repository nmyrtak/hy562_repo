package csd562.csdp1092.exercise1;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import csd562.csdp1092.Shared.IntWritableCustom;
import csd562.csdp1092.Shared.Utils;

public class StopWordsFinder {
	
    public static class TokenizeMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();
        
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        	String delimiter = " !@#$%^&*()-_=+,./?:;'\"|{}[]~`<>\t\r\n";
        	StringTokenizer strtok = new StringTokenizer(value.toString(), delimiter);
        	while(strtok.hasMoreTokens()){
        		word.set(strtok.nextToken().toLowerCase());
        		if(!Character.isLetter(word.charAt(0)))
        			continue;
        		context.write(word, one);
        	}
        }
    }
    
    public static class KeyValueReverseMapper extends Mapper<LongWritable, Text, IntWritableCustom, Text>{
    	private Text word = new Text();
    	private IntWritableCustom freq = new IntWritableCustom();
    	
    	    	
    	@Override
    	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
    		int wordIndex = 0;
    		int freqIndex = 1;
    		String[] lineParts = value.toString().split("[ \t\n]");
    		word.set(lineParts[wordIndex]);
    		System.out.println();
    		freq.set(Integer.parseInt(lineParts[freqIndex]));
    		context.write(freq, word);
    	}
    }
    
    public static class WriteByValueReducer extends Reducer<IntWritableCustom, Text, Text, IntWritable>{
        private Text result = new Text();
        private int threshold = 4000;

    	@Override
    	protected void reduce(IntWritableCustom key, Iterable<Text> values,Context context) throws IOException, InterruptedException {
    		if(key.get() <= threshold)
    			return;
    		for(Text value : values){
    			result.set(value);
    			context.write(result, key);
    		}
    	}
    }

    public static class IntSumReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
        private IntWritable result = new IntWritable();
        
        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;
        	for(IntWritable value : values){
            	sum += value.get();
            }
        	result.set(sum);
        	context.write(key, result);
        }
    }

    public static void main(String[] args) throws Exception {
    	
    	String wordCountOutput = "wordsCountOutput";
    	String stopWordsOutput = args[1];
    	String stopWordsCsv = "stopwords.csv";
    	
    	Configuration conf = new Configuration();
    
    	FileSystem fs = FileSystem.get(conf);
    	fs.delete(new Path(wordCountOutput));
    	fs.delete(new Path(stopWordsOutput));
    	
    	Job wordCountJob = new Job(conf, "word count");
    	wordCountJob.setJarByClass(StopWordsFinder.class);
    	wordCountJob.setMapperClass(TokenizeMapper.class);
    	wordCountJob.setCombinerClass(IntSumReducer.class);
    	wordCountJob.setReducerClass(IntSumReducer.class);
    	wordCountJob.setOutputKeyClass(Text.class);
    	wordCountJob.setOutputValueClass(IntWritable.class);
    	FileInputFormat.addInputPath(wordCountJob, new Path(args[0]));
    	FileOutputFormat.setOutputPath(wordCountJob, new Path(wordCountOutput));
        boolean successWC = wordCountJob.waitForCompletion(true);
        
        if(!successWC)
        	System.exit(1);
        
        conf.set("mapred.textoutputformat.separator", ",");
        Job stopWordsFinderJop = new Job(conf, "stopwords finder");
        stopWordsFinderJop.setJarByClass(StopWordsFinder.class);
        stopWordsFinderJop.setMapperClass(KeyValueReverseMapper.class);
        stopWordsFinderJop.setReducerClass(WriteByValueReducer.class);
        stopWordsFinderJop.setOutputKeyClass(IntWritableCustom.class);
        stopWordsFinderJop.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(stopWordsFinderJop, new Path(wordCountOutput));
    	FileOutputFormat.setOutputPath(stopWordsFinderJop, new Path(stopWordsOutput));
        boolean successSW = stopWordsFinderJop.waitForCompletion(true);
        
        if(!successSW)
        	System.exit(1);
        
        Utils utils = new Utils();
        utils.printOutputFolder(new File(stopWordsOutput), 10);
        utils.storeInCSV(conf, new Path(stopWordsOutput + "/part-r-00000"), new Path(stopWordsCsv));
        
        fs.delete(new Path(wordCountOutput));
        
        fs.close();
        
    }
    
    
    
    
    
}
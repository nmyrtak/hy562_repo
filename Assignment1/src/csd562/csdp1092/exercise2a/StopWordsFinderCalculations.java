package csd562.csdp1092.exercise2a;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import csd562.csdp1092.Shared.IntWritableCustom;
import csd562.csdp1092.Shared.Utils;

public class StopWordsFinderCalculations {
	
    public static class TokenizeMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();
        
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        	String delimiter = " !@#$%^&*()-_=+,./?:;'\"|{}[]~`<>\t\r\n";
        	StringTokenizer strtok = new StringTokenizer(value.toString(), delimiter);
        	while(strtok.hasMoreTokens()){
        		word.set(strtok.nextToken().toLowerCase());
        		context.write(word, one);
        	}
        }
    }
    
    public static class KeyValueReverseMapper extends Mapper<LongWritable, Text, IntWritableCustom, Text>{
    	private Text word = new Text();
    	private IntWritableCustom freq = new IntWritableCustom();
    	
    	    	
    	@Override
    	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
    		int wordIndex = 0;
    		int freqIndex = 1;
    		String[] lineParts = value.toString().split("[ \t\n]");
    		word.set(lineParts[wordIndex]);
    		System.out.println();
    		freq.set(Integer.parseInt(lineParts[freqIndex]));
    		context.write(freq, word);
    	}
    }
    
    public static class WriteByValueReducer extends Reducer<IntWritableCustom, Text, Text, IntWritable>{
        private Text result = new Text();
        private int threshold = 4000;

    	@Override
    	protected void reduce(IntWritableCustom key, Iterable<Text> values,Context context) throws IOException, InterruptedException {
    		if(key.get() <= threshold)
    			return;
    		for(Text value : values){
    			result.set(value);
    			context.write(result, key);
    		}
    	}
    }

    public static class IntSumReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
        private IntWritable result = new IntWritable();
        
        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;
        	for(IntWritable value : values){
            	sum += value.get();
            }
        	result.set(sum);
        	context.write(key, result);
        }
    }

    public static void main(String[] args) throws Exception {
    	
    	String wordCountOutput = "wordsCountOutput";
    	String stopWordsOutput = args[1];
    	String stopWordsCsv = "stopwords.csv";
    	
    	
        
        calcAvgTimes("ex2a_1", wordCountOutput, stopWordsOutput, args[0]);
        calcAvgTimes("ex2a_2", wordCountOutput, stopWordsOutput, args[0]); 
        calcAvgTimes("ex2a_3", wordCountOutput, stopWordsOutput, args[0]); 
        calcAvgTimes("ex2a_4", wordCountOutput, stopWordsOutput, args[0]);

        
       //Utils utils = new Utils();
        //utils.printOutputFolder(new File(stopWordsOutput), 10);
      //  utils.storeInCSV(conf, new Path(stopWordsOutput + "/part-r-00000"), new Path(stopWordsCsv));
        
     
        
    }
    
    private static void calcAvgTimes(String exSubQuest, String wordCountOutput,
    		String stopWordsOutput, String input) throws Exception{
    	double avgTime = 0;
    	int runs = 10;
    	for(int i = 0; i < runs; i++){
        	if(exSubQuest.equals("ex2a_1"))
        		avgTime += Driver(wordCountOutput, stopWordsOutput, input, 10, false, false);
        	else if(exSubQuest.equals("ex2a_2"))
        		avgTime += Driver(wordCountOutput, stopWordsOutput, input, 10, false, true);
        	else if(exSubQuest.equals("ex2a_3"))
        		avgTime += Driver(wordCountOutput, stopWordsOutput, input, 10, true, true);
        	else if(exSubQuest.equals("ex2a_4"))
        		avgTime += Driver(wordCountOutput, stopWordsOutput, input, 50, true, true);
    	}
    	String res = "Average time for " + exSubQuest + " is " + (avgTime / runs) +  " ms";
    	new Utils().execCommand("echo \"" + res + "\" >> ./avgTimes.txt" );
    	System.out.println(res);
    }
    
    private static double Driver(String wordCountOutput, String stopWordsOutput,
    		String input, int numReducers, boolean useCompression, boolean useCombiner) throws Exception{
    	double timeStart = System.currentTimeMillis() / 1000.0;
    	
    	
    	Configuration conf = new Configuration();
    	
    	if(useCompression){
    		conf.setBoolean("mapred.compress.map.output", true); 
    		conf.set("mapred.map.output.compression.codec", "org.apache.hadoop.io.compress.SnappyCodec");
    	}

    	FileSystem fs = FileSystem.get(conf);
    	fs.delete(new Path(wordCountOutput));
    	fs.delete(new Path(stopWordsOutput));
    	
    	Job wordCountJob = new Job(conf, "word count");
    	wordCountJob.setNumReduceTasks(numReducers);
    	wordCountJob.setJarByClass(StopWordsFinderCalculations.class);
    	wordCountJob.setMapperClass(TokenizeMapper.class);
    	if(useCombiner)
    		wordCountJob.setCombinerClass(IntSumReducer.class);
    	wordCountJob.setReducerClass(IntSumReducer.class);
    	wordCountJob.setOutputKeyClass(Text.class);
    	wordCountJob.setOutputValueClass(IntWritable.class);
    	FileInputFormat.addInputPath(wordCountJob, new Path(input));
    	FileOutputFormat.setOutputPath(wordCountJob, new Path(wordCountOutput));
        boolean successWC = wordCountJob.waitForCompletion(true);
        
    	//double firstJobTime = System.currentTimeMillis() / 1000.0 - timeStart;
        
        if(!successWC)
        	System.exit(1);
        
        conf.set("mapred.textoutputformat.separator", ",");
        Job stopWordsFinderJop = new Job(conf, "stopwords finder");
        stopWordsFinderJop.setJarByClass(StopWordsFinderCalculations.class);
        stopWordsFinderJop.setMapperClass(KeyValueReverseMapper.class);
        stopWordsFinderJop.setReducerClass(WriteByValueReducer.class);
        stopWordsFinderJop.setOutputKeyClass(IntWritableCustom.class);
        stopWordsFinderJop.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(stopWordsFinderJop, new Path(wordCountOutput));
    	FileOutputFormat.setOutputPath(stopWordsFinderJop, new Path(stopWordsOutput));
        boolean successSW = stopWordsFinderJop.waitForCompletion(true);
        
        if(!successSW)
        	System.exit(1);
    	
    	
    	double totalTimePassed = System.currentTimeMillis() / 1000.0 - timeStart;
    	
	   fs.delete(new Path(wordCountOutput));
       
       fs.close();
    	
    	return totalTimePassed;
    	
    }
    
    
    
}
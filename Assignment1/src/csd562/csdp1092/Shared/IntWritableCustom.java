package csd562.csdp1092.Shared;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class IntWritableCustom extends IntWritable {

	@Override
	public int compareTo(IntWritable o) {
		return (this.get() < o.get() ? 1 : (this.get() == o.get() ? 0 : -1));
	}
	
}

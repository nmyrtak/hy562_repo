package csd562.csdp1092.Shared;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class Utils {
	
	
	public void printOutputFolder(File outputFolder){
		String catCommand = "cat " + outputFolder.getAbsolutePath() + "/*";
		System.out.println("\n" + catCommand);
		execCommand(catCommand);
	}
	
	public void printOutputFolder(File outputFolder, int k){
		String catCommand = "cat " + outputFolder.getAbsolutePath() + "/* | head -n " + k;
		System.out.println("\n" + catCommand);
		execCommand(catCommand);
	}
	
	public void storeInCSV(Configuration conf, Path sourceFilePath, Path targetFilePath){
		try {
			FileSystem fs = FileSystem.get(conf);
			Path tmpPath = new Path("stopwords.csv");
			FSDataInputStream fsdi = fs.open(sourceFilePath);
			FSDataOutputStream fsdo = fs.create(tmpPath);
			String line = "";
			while((line = fsdi.readLine()) != null)
				fsdo.writeBytes(line + "\n");
			fsdi.close();
			fsdo.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void storeUnqCounter(Configuration conf, long counter){
		try {
			FileSystem fs = FileSystem.get(conf);
			FSDataOutputStream fsdo;
			fsdo = fs.create(new Path("UNQ_COUNTER.txt"));
			fsdo.writeBytes(counter + "");
			fsdo.close();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void execCommand(String command){
		try{
            ProcessBuilder pb = new ProcessBuilder("bash", "-c", command);
            pb.redirectErrorStream(true);
            Process process = pb.start();
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = "";
            while ((line = br.readLine()) != null)
                System.out.println(line);
            br.close();
        } catch (Exception e) { e.printStackTrace(); }
	}

}

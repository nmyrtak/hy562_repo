package csd562.csdp1092.exercise0;

import java.io.File;
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import csd562.csdp1092.Shared.Utils;

public class WordCount {
	
    public static class TokenizeMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();
        
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        	StringTokenizer strtok = new StringTokenizer(value.toString());
        	while(strtok.hasMoreTokens()){
        		word.set(strtok.nextToken());
        		context.write(word, one);
        	}
        }
    }

    public static class IntSumReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
        private IntWritable result = new IntWritable();
        
        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;
        	for(IntWritable value : values){
            	sum += value.get();
            }
        	result.set(sum);
        	context.write(key, result);
        }
    }

    public static void main(String[] args) throws Exception {
    	Configuration conf = new Configuration();
    	
    	FileSystem fs = FileSystem.get(conf);
    	fs.delete(new Path(args[1]));
    	
    	Job job = new Job(conf, "word count");
    	job.setJarByClass(WordCount.class);
    	job.setMapperClass(TokenizeMapper.class);
    	job.setCombinerClass(IntSumReducer.class);
    	job.setReducerClass(IntSumReducer.class);
    	job.setOutputKeyClass(Text.class);
    	job.setOutputValueClass(IntWritable.class);
    	FileInputFormat.addInputPath(job, new Path(args[0]));
    	FileOutputFormat.setOutputPath(job, new Path(args[1]));
    	
        boolean success = job.waitForCompletion(true);
        if(!success)
        	System.exit(1);
        
        Utils utils = new Utils();
        utils.printOutputFolder(new File(args[1]));
        fs.close();
    }
}
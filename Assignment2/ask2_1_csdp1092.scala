import org.apache.spark.rdd.RDD

class WebLog (_projectCode:String, _pageTitle:String, _numOfHits:Long, _pageSize:Long) extends Serializable{
var projectCode : String = _projectCode;
var pageTitle : String = _pageTitle;
var numOfHits : Long = _numOfHits;
var pageSize : Long = _pageSize;

override def toString: String =
    s"$projectCode $pageTitle $numOfHits $pageSize"
}

def getWebLog(line:String) : WebLog = {
val lineParts : Array[String] = line.split(" "); 
val projectCode = lineParts.lift(0).get;
val pageTitle = lineParts.lift(1).get;
val numOfHits = lineParts.lift(2).get.toLong;
val pageSize = lineParts.lift(3).get.toLong;
return new WebLog(projectCode, pageTitle, numOfHits, pageSize);
}

val pagecounts = sc.textFile("./pagecounts-20160101-000000_parsed.out").map(l => getWebLog(l))


// ============== Ερώτημα 1. ==============

def printerK(rdd:RDD[WebLog], k:Int) = {
rdd.take(k).foreach(println)
}

printerK(pagecounts, 15)


// ============== Ερώτημα 2. ==============

def getTotalRecords(rdd:RDD[WebLog]) : Long = {
return rdd.count()
}

val totalRecs = getTotalRecords(pagecounts)
println(totalRecs)


// ============== Ερώτημα 3. ==============

def getMaxPageSize(rdd:RDD[WebLog]) : Long = {
return rdd.map(_.pageSize).reduce(_ max _)
}

println("Max page size is " + getMaxPageSize(pagecounts))

def getMinPageSize(rdd:RDD[WebLog]) : Long = {
return rdd.map(_.pageSize).reduce(_ min _)
}

println("Min page size is " + getMinPageSize(pagecounts))

def getAvgPageSize(rdd:RDD[WebLog]) : Double = {
return rdd.map(_.pageSize).reduce(_ + _).toDouble / getTotalRecords(pagecounts).toDouble
}

println("Average page size is " + getAvgPageSize(pagecounts))


// ============== Ερώτημα 4. ==============

def getMaxPageRecs(rdd:RDD[WebLog]): RDD[WebLog] = {
val maxPageSize = getMaxPageSize(rdd)
return rdd.filter(l => l.pageSize == maxPageSize)
}

getMaxPageRecs(pagecounts).foreach(println)


// ============== Ερώτημα 5. ==============

def getMostPopRecOfMaxPageSize(rdd:RDD[WebLog]): WebLog = {
val maxPageRecs = getMaxPageRecs(rdd)
return maxPageRecs.sortBy(_.numOfHits, false).first()
}

print("Most popular record of those with max page sizes is: " + getMostPopRecOfMaxPageSize(pagecounts))


// ============== Ερώτημα 6. ==============

def getMaxTitleLen(rdd:RDD[WebLog]) : RDD[WebLog] = {
val maxTitleLen = rdd.map(l => l.pageTitle.length).max()
return rdd.filter(l => l.pageTitle.length == maxTitleLen)
}

getMaxTitleLen(pagecounts).foreach(println)


// ============== Ερώτημα 7. ==============

def getRddWithPagesGTavg(rdd:RDD[WebLog]) : RDD[WebLog] = {
val avgPageSize = getAvgPageSize(rdd)
return rdd.filter(l => l.pageSize > avgPageSize)
}

val rddAvg = getRddWithPagesGTavg(pagecounts)


// ============== Ερώτημα 8. ==============

def totalPageViewsPerProj(rdd:RDD[WebLog]) : RDD[(String, Long)] = {
return rdd.map(l => (l.projectCode, l.numOfHits)).reduceByKey(_ + _)
}

val projTotalHits = totalPageViewsPerProj(pagecounts)
projTotalHits.take(10).foreach(println)


// ============== Ερώτημα 9. ==============

def printTop10Projects(rdd:RDD[(String, Long)]) = {
rdd.sortBy(_._2, false).take(10).foreach(println)
}

printTop10Projects(projTotalHits)


// ============== Ερώτημα 10. ==============

def printNotEnProjectsWithPrefixNum(rdd:RDD[WebLog], projCode:String, prefix:String) = {
val startWithPrefix = rdd.filter(l => l.pageTitle.startsWith("The")) 
println("Page titles stating with 'The' are " + startWithPrefix.count)
val notPartOfProject = startWithPrefix.filter(l => !l.projectCode.contains("en"))
println("Page titles stating with 'The' and they aren't part of en are " + notPartOfProject.count)
}

printNotEnProjectsWithPrefixNum(pagecounts, "en", "The") 


// ============== Ερώτημα 11. ==============

def singleViewPages(rdd:RDD[WebLog]) : Double = {
val singleViews = rdd.filter(l => l.numOfHits == 1).count()
return singleViews.toDouble / rdd.count().toDouble * 100
}

println(singleViewPages(pagecounts))


// ============== Ερώτημα 12. ==============

def refineWord(word:String) : String = {
return word.toLowerCase().replaceAll("[^A-Za-z0-9]","")
}

def getUnqTitleTermsNum(rdd:RDD[WebLog]) : Long = {
val termsMap = rdd.flatMap(l => l.pageTitle.split("_"))
return termsMap.map(w => refineWord(w.toString)).filter(!_.isEmpty()).distinct.count()
}

val unqTerms = getUnqTitleTermsNum(pagecounts)
println("The unique terms in titles are " + unqTerms)


// ============== Ερώτημα 13. ==============

def getMostFreqTitle(rdd:RDD[WebLog]) : (String, Long) = {
val termsFreqMap = rdd.map(w => (w.pageTitle, 1L)).reduceByKey(_ + _)
return termsFreqMap.sortBy(_._2, false).first()
}

val mostFreqTitle = getMostFreqTitle(pagecounts)
println("Most frequently occurring page title is " + mostFreqTitle)


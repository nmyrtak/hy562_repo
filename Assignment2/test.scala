import org.apache.spark.rdd.RDD

val pagecounts = sc.textFile("./pagecounts-20160101-000000_parsed.out")

val projectCodeIndex = 0;
val pageTitleIndex = 1;
val numOfHitsIndex = 2;
val pageSizeIndex = 3;

// 12

def refineWord(word:String) : String = {
return word.toLowerCase().replaceAll("[^A-Za-z0-9]","")
}

def getUnqTitleTermsNum(rdd:RDD[String], pageTitleIndex:Int) : Long = {
val termsMap = rdd.flatMap(l => l.split(" ").lift(pageTitleIndex).get.split("_"))
return termsMap.map(w => refineWord(w.toString)).filter(!_.isEmpty()).distinct.count()
}

val unqTerms = getUnqTitleTermsNum(pagecounts, pageTitleIndex)
println("The unique terms in titles are " + unqTerms)

// 13

def getMostFreqTitleTerm(rdd:RDD[String], pageTitleIndex:Int) : (String, Long) = {
val termsMap = rdd.flatMap(l => l.split(" ").lift(pageTitleIndex).get.split("_"))
val termsFreqMap = termsMap.map(w => (refineWord(w.toString), 1L)).reduceByKey(_ + _)
return termsFreqMap.collect().maxBy(_._2)
}

val mostFreqTerm = getMostFreqTitleTerm(pagecounts, pageTitleIndex)